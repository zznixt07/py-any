from setuptools import setup

setup(name='py_any',
    version=0.1,
    description='',
    url='https://zznixt.me',
    author='zznixt',
    author_email='zznixt07@protonmail.com',
    license='MIT',
    packages=[
        'py_any',
        'py_any.utils',
    ],
    install_requires=[
        'requests',
        'beautifulsoup4',
        'cookies_jar @ git+https://gitlab.com/zznixt07/cookies-jar.git',
    ],
    zip_safe=False
)