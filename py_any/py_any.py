import sys
import logging
import traceback
from functools import cached_property, wraps
from datetime import datetime, time, timezone, timedelta
from json import JSONDecodeError
import requests
from requests.exceptions import RequestException
from cookies_jar import CookiesJar

logger = logging.getLogger(__name__)

def func_logger(func=None, fallback_func=None):
    '''A decorator which
    -> logs which function was called,
    -> creates a logger named __name__.func_name 
        and passes it under named argument `logger` to the function
    -> catches exceptions if callback_function is passed which calls
        callback_function with 3 named arguments `exc_type`, `exc_value` & `exc_trackeback`
        that has traceback related info.
        Traceback can be retrieved as string with:
            '\n'.join(traceback.format_exception(exc_type, exc_value, exc_traceback))
    '''
    
    def wrapper_func_logger(func):
        @wraps(func)
        def wrapper_wrapper_func_logger(*args, **kwargs):
            fn = func.__name__
            fn_logger = logging.getLogger(__name__ + '.' + fn)
            fn_logger.debug('Function <%s> called.', fn)
            try:
                return func(*args, logger=fn_logger, **kwargs)
            except Exception as err:
                error = traceback.format_exc()
                logger.error('Error:\n%s\n%s', err, error)
                if fallback_func:
                    ex = sys.exc_info()
                    return fallback_func(exc_type=ex[0], exc_value=ex[1], exc_traceback=ex[2])
                raise err
        return wrapper_wrapper_func_logger
    if func:
        return wrapper_func_logger(func)
    return wrapper_func_logger


class PythonAnywhere:
    """browser API."""

    UA = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'
    origin = 'https://www.pythonanywhere.com'
    referer = origin

    def __init__(self, username: str, passwd: str, fresh_login: bool = False):
        """cookie is always stored if creds is correct. Set fresh_login to True
        to ignore saved cookie and login in everytime."""

        self.username = username
        self.task_url = f'{self.origin}/user/{self.username}/tasks_tab/'
        self.task_url_referer = f'{self.referer}/user/{self.username}/tasks_tab/'
        
        self.sess = requests.Session()
        self.sess.headers.update({'User-Agent': self.UA})

        jar = CookiesJar('pyanywhere_' + self.username)
        cookies = jar.get_cookie('sessionid')

        if (cookies and not fresh_login) and self.is_logged_in():
            logger.debug('using cookies')
            self.sess.cookies.set(name='sessionid', value=cookies)
        else:
            # bs4 needed here
            from bs4 import BeautifulSoup

            logger.debug('cookie expired/not present. logging in using creds.')
            resp = self.sess.get(self.origin + '/login/')
            soup = BeautifulSoup(resp.content, 'lxml')
            csrf = soup.select_one('form input[name=csrfmiddlewaretoken]').get('value')
            payload = {
                'csrfmiddlewaretoken': csrf,
                'login_view-current_step': 'auth',
                'auth-username': username,
                'auth-password': passwd
            }
            url = self.origin + '/login/'
            resp = self.sess.post(
                url,
                data=payload,
                headers={'Referer': self.referer + '/login/'},
                # cookies=cookies
            )
            
            # cannot use resp.ok. see func<is_logged_in>.
            if self.is_logged_in:   # only store cookie when login succeeds.
                for cookie in self.sess.cookies:
                    if cookie.name == 'sessionid':
                        jar.put_cookie('sessionid', cookie.value, cookie.expires)
                        break
            else:
                logger.error('Login failed.')
                logger.error('STATUS: %s', resp.status_code)
                logger.error('REQ URL: %s', url)
                logger.error('REQ URL(request.url): %s', resp.request.url)
                logger.error('REQ HEADER: %s', resp.request.headers)
                for i, hist in enumerate(resp.history):
                    # logger.error('REQ HEADER[%d]: %s', i, hist.headers)
                    logger.error('%s Cookies[%d]: %s', hist.url, i, hist.cookies)
                logger.error('REQ PAYLOAD: %s', payload)
                logger.error('RESP: %s', resp.text)

    @cached_property
    def is_logged_in(self):
        '''after sending creds; server sends 200 on incorrect creds and
        302 on login success which is not the best response.
        there is a more robust way as below.'''

        resp = self.sess.get(self.origin + '/loggedin/', allow_redirects=False)
        loc = resp.headers.get('Location')
        if loc and f'/user/{self.username}/' in loc:
            return True
        return False

    @cached_property
    def csrf_for_task(self):
        logger.debug('extracting csrf from tasks form')
        # bs4 may not be imported in __init__ cuz its under a conditional stmt.
        from bs4 import BeautifulSoup
        soup = BeautifulSoup(self.sess.get(self.task_url).content, 'lxml')
        x_csrf_token = soup.select_one('input[name=csrfmiddlewaretoken]').get('value')
        # this header is same in activities inside tasks tab
        return x_csrf_token

    def extend_expiry_date(self):
        "extends expiry date of all tasks"
        
        self.sess.headers.update({
            'Referer': self.task_url_referer,
            'X-CSRFToken': self.csrf_for_task
        })
        # view schedule details
        schd_url = f'{self.origin}/api/v0/user/{self.username}/schedule/'
        # extend expiry date using relative url to endpoint present in key `extend_url`
        for data in self.sess.get(schd_url).json():
            resp = self.sess.post(self.origin + data['extend_url'], json=None)
            logger.debug('extend schd. task response: %s', resp.text)


class PythonAnywhereApi:
    """this class uses the official pythonanywhere public API"""

    origin = 'https://www.pythonanywhere.com'
    retried = 0

    def __init__(self, username: str, api_token: str):
        self.sess = requests.Session()
        self.sess.headers['Authorization'] = f'Token {api_token}'
        self.api_url = self.origin + '/api/v0/user/' + username
        self.username = username

    def repeat_tasks(self, every_hh: int = 1, every_mm: int = 0) -> bool:
        "schedule multiple tasks if available"

        try:
            tasks = self.sess.get(self.api_url + '/schedule/').json()
        except (RequestException, JSONDecodeError):
            return bool(
                self.exponential_backoff_and_recall(
                    self.repeat_tasks, every_hh, every_mm
            ))

        if not tasks:
            logger.debug('you have no tasks scheduled.')
            return

        for task in tasks:
            task_id = task['id']
            cmd = task['command']
            future_time = (
                self.curr_server_time()
                + timedelta(hours=every_hh, minutes=every_mm)
            ).timetz()
            if not self.repeat_task(task_id, future_time, cmd):
                return False

        return True

    def repeat_task(self, task_id: int, future_time: time, cmd: str) -> bool:
        "schedule only one task"

        payload = {
            'command': cmd,
            'hour': future_time.hour,               # accepts both 05 and 5
            'minute': future_time.minute,
            'enabled': True,
            'interval': 'daily',
        }
        try:
            just_sched_info = self.sess.patch(
                f'{self.api_url}/schedule/{task_id}/', json=payload
            ).json()
        except (RequestException, JSONDecodeError):
            return bool(
                self.exponential_backoff_and_recall(
                    self.repeat_task, task_id, future_time, cmd
            ))

        sched_for = datetime.strptime(just_sched_info['printable_time'], '%H:%M')

        logger.debug(
            'sched. for(NP): %s',
            (sched_for + timedelta(hours=5, minutes=45)).strftime('%H:%M'))
        logger.debug('sched. for(UTC): %s', sched_for.strftime('%H:%M'))

        if not PythonAnywhereApi.task_sched_on_time(future_time, just_sched_info):
            logger.debug('task was not scheduled correctly. retrying..')
            # recursively call itself.
            return self.repeat_task(task_id, future_time, cmd)

        return True
    
    def exponential_backoff_and_recall(self, method, *args, **kwargs):
        from time import sleep
        import random

        PythonAnywhereApi.retried += 1
        if self.retried > 10:
            return None
        sleep(2 ** (self.retried * random.uniform(0.4, 0.9)))
        return method(*args, **kwargs)

    def upload_file(self, src_path: str, dest_path: str) -> None:
        "Note: Not a problem if the file to be overwritten is currently running."

        file = [
            ('content', ('name', open(src_path, 'rb')))
        ]
        resp = self.sess.post(f'{self.api_url}/files/path{dest_path}', files=file)
        status = resp.status_code
        if status == 200:
            print('overwritten file:', dest_path)
        elif status == 201:
            print('created file:    ', dest_path)

    def delete_file(self, dest_path: str) -> bool:
        resp = self.sess.delete(f'{self.api_url}/files/path{dest_path}')
        return resp.status_code == 204

    def curr_server_time(self):
        return datetime.now(timezone.utc)

    def get_consoles(self):
        return self.sess.get(self.api_url + '/consoles').json()

    def create_console(self, exec='bash', args='', wd=None):
        return self.sess.post(
            self.api_url + '/consoles/',
            data={
                'executable': exec,
                'arguments': args,
                'working_directory': wd or ('/home/' + self.username)
            }
        ).json()

    @staticmethod
    def task_sched_on_time(tm, details):
        '''confirms if task was scheduled successfully.'''

        return tm.hour == details['hour'] and tm.minute == details['minute']


if __name__ == '__main__':
    import os
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logger = logging.getLogger(__name__)
    PY_ANY_USER = ''
    PY_ANY_PASS = ''
    PY_ANY_API_TOKEN = ''

    parent_dir = os.path.dirname(os.path.abspath(__file__))

    py_api = PythonAnywhereApi(PY_ANY_USER, PY_ANY_API_TOKEN)
    # py_api.upload_file(
    #     os.path.join(parent_dir, 'routines.json'),
    #     f'/home/{PY_ANY_USER}/routines.json'
    # )
    print(py_api.repeat_tasks(every_hh=0, every_mm=12))
    
    pyany = PythonAnywhere(PY_ANY_USER, PY_ANY_PASS)
    pyany.extend_expiry_date()

    print('GG')