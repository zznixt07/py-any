import os
import json
from functools import wraps
from datetime import datetime, timedelta, timezone
import logging

logger = logging.getLogger(__name__)

def get_datadir() -> str:
    import platform
    """
    # linux: ~/.local/share
    # macOS: ~/Library/Application Support
    # windows: C:/Users/<USER>/AppData/Roaming
    """

    home = os.path.expanduser('~')
    platform_name = platform.system().lower()
    if 'windows' in platform_name:
        datadir = 'AppData/Roaming'
    if 'linux' in platform_name:
        datadir = '.local/share'
    elif 'darwin' in platform_name:
        datadir = 'Library/Application Support'
    # don think we can make the dir if for some strange reason its doesn't exists
    return os.path.join(home, datadir)


def get_default(key, value, overwrite=False):

    dir_path = os.path.join(get_datadir(), 'py_my_db', 'db')
    os.makedirs(dir_path, exist_ok=True)
    with open(os.path.join(dir_path, 'fn_ts.json'), 'a+', encoding='UTF-8') as file:
        file.seek(0)
        contents = file.read()
        
        try:
            obj = json.loads(contents)
        except json.JSONDecodeError:
            obj = {}

        if not overwrite and key in obj:
            return obj[key]
        obj[key] = value

        file.seek(0)
        file.truncate()
        file.write(json.dumps(obj))
        return value

def every(func=None, *, days=3):
    '''param::days also accepts float cuz timedelta supports float.
    timestamp is stored in UTC not local.
    '''

    def wrapper(fn):
        
        @wraps(fn)
        def call_fn_or_not(*args, **kwargs):
            fn_name = fn.__name__
            now_ts = datetime.now(tz=timezone.utc).timestamp()
            t_minus_days = now_ts + timedelta(days=days).total_seconds()
            stored_ts = get_default(fn_name, t_minus_days)
            # this is the obv way to check if value was written or not by above func.
            if stored_ts != t_minus_days:
                # if its a new entry, dont enter this loop.
                if now_ts > stored_ts:
                    # expired
                    get_default(fn_name, t_minus_days, overwrite=True)
                else:
                    # now_ts <= stored_ts; hasn't expired
                    logger.info('skipping %s. expires in %.0f secs', fn_name, stored_ts - now_ts)
                    return
            logger.info('running %s', fn_name)
            return fn(*args, **kwargs)

        return call_fn_or_not

    if func:
        return wrapper(func)
    return wrapper


if __name__ == '__main__':
    logging.basicConfig(level=10)
    logger = logging.getLogger(__name__)

    @every
    def a(): print('=====')

    a()